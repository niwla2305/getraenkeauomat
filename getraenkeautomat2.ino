int Beleuchtung = 9;
int ColaPump = 8;
int FantaPump = 7;
int WasserPump = 6;
int Wasser2Pump = 5;
int ApfelsaftPump = 4;

int ColaBut = 12;
int SpeziBut = 16; //A2
int FantaBut = 11;
int WasserBut = 18; //A4
int Wasser2But = 10;
int ApfelsaftBut = 14; //A0
int ApfelschorleBut = 15; //A1
int Apfelschorle2But = 19; //A5

bool running = false;
int bigsize = 28000;
int normalsize = 15000;

void setup() {
  Serial.begin(9600);
  
  pinMode(Beleuchtung, OUTPUT);

  pinMode(ColaPump, OUTPUT);
  pinMode(FantaPump, OUTPUT);
  pinMode(WasserPump, OUTPUT);
  pinMode(Wasser2Pump, OUTPUT);
  pinMode(ApfelsaftPump, OUTPUT);

  pinMode(ColaBut, INPUT_PULLUP);
  pinMode(SpeziBut, INPUT_PULLUP);
  pinMode(FantaBut, INPUT_PULLUP);
  pinMode(WasserBut, INPUT_PULLUP);
  pinMode(Wasser2But, INPUT_PULLUP);
  pinMode(ApfelsaftBut, INPUT_PULLUP);
  pinMode(ApfelschorleBut, INPUT_PULLUP);
  pinMode(Apfelschorle2But, INPUT_PULLUP);

  digitalWrite(ColaPump, HIGH);
  digitalWrite(FantaPump, HIGH);
  digitalWrite(WasserPump, HIGH);
  digitalWrite(Wasser2Pump, HIGH);
  digitalWrite(ApfelsaftPump, HIGH);

  digitalWrite(Beleuchtung, HIGH);
  Serial.begin(9600);

}



void loop() {
  if (running == false)
  {
    if (digitalRead(ColaBut) == HIGH)
    {
      digitalWrite(ColaPump, LOW);
      digitalWrite(Beleuchtung, LOW);
      running = true;
    }
    else if (digitalRead(SpeziBut) == HIGH)
    {
      digitalWrite(FantaPump, LOW);
      digitalWrite(ColaPump, LOW);
      digitalWrite(Beleuchtung, LOW);
      running = true;
    }
    else if (digitalRead(FantaBut) == HIGH)
    {
      digitalWrite(FantaPump, LOW);
      digitalWrite(Beleuchtung, LOW);
      running = true;
    }
    else if (digitalRead(WasserBut) == HIGH)
    {
      digitalWrite(WasserPump, LOW);
      digitalWrite(Beleuchtung, LOW);
      running = true;
    }
    else if (digitalRead(Wasser2But) == HIGH)
    {
      digitalWrite(Wasser2Pump, LOW);
      digitalWrite(Beleuchtung, LOW);
      running = true;
    }
    else if (digitalRead(ApfelsaftBut) == HIGH)
    {
      digitalWrite(ApfelsaftPump, LOW);
      digitalWrite(Beleuchtung, LOW);
      running = true;
    }
    else if (digitalRead(ApfelschorleBut) == HIGH)
    {
      digitalWrite(ApfelsaftPump, LOW);
      digitalWrite(WasserPump, LOW);
      digitalWrite(Beleuchtung, LOW);
      running = true;
    }
    else if (digitalRead(Apfelschorle2But) == HIGH)
    {
      digitalWrite(ApfelsaftPump, LOW);
      digitalWrite(Wasser2Pump, LOW);
      digitalWrite(Beleuchtung, LOW);
      running = true;
    }
    delay(1000);
  }
  else if (running==true)
  {
    if ( digitalRead(ColaBut) == HIGH ||  digitalRead(SpeziBut)  == HIGH||  digitalRead(FantaBut) == HIGH ||  digitalRead(WasserBut) == HIGH ||  digitalRead(Wasser2But) == HIGH ||  digitalRead(ApfelsaftBut) == HIGH ||  digitalRead(ApfelschorleBut) == HIGH ||  digitalRead(Apfelschorle2But) == HIGH)
     {
       digitalWrite(ColaPump, HIGH);
       digitalWrite(FantaPump, HIGH);
       digitalWrite(ApfelsaftPump, HIGH);
       digitalWrite(WasserPump, HIGH);
       digitalWrite(Wasser2Pump, HIGH);
       digitalWrite(Beleuchtung, HIGH);
       running = false;
       delay(1000);
     }
  }
}




  // else if (running==true)
  // {
  //   
  // }